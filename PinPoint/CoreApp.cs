﻿using System;
using MvvmCross.Core.ViewModels;
using MvvmCross.Platform;
using MvvmCross.Platform.IoC;
using PinPoint.Services;
using PinPoint.ViewModels;
using PinPoint.Helpers;

namespace PinPoint
{
    public class CoreApp : MvxApplication
    {
        public override void Initialize()
        {
            CreatableTypes()
                .EndingWith("Service")
                .AsInterfaces()
                .RegisterAsLazySingleton();

            RegisterNavigationServiceAppStart<LoginViewModel>();


            Mvx.RegisterType<IDialogService, DialogService>();

            Mvx.RegisterType<IApiService, ApiService>();



        }
    }
}
