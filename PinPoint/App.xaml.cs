﻿using System;
using MvvmCross.Forms.Platform;
using Plugin.FirebasePushNotification;
using Xamarin.Forms;
using PinPoint.Helpers;
using PinPoint.Pages;
using PinPoint.ViewModels;
using MvvmCross.Platform;
using PinPoint.Services;

namespace PinPoint
{
    public partial class App : MvxFormsApplication
    {
        public const string AppName = "PinPoint";

        public int BadgeCount = 0;

        public App()
        {
            InitializeComponent();
        }

        protected async override void OnStart()
        {
            // Handle when your app starts

            CrossFirebasePushNotification.Current.Subscribe("general");
            CrossFirebasePushNotification.Current.OnTokenRefresh += (s, p) =>
            {
                System.Diagnostics.Debug.WriteLine($"TOKEN REC: {p.Token}");
                AppSettings.FirebaseToken = p.Token;
                Mvx.Resolve<IPlatformService>().Log("TOKENCHANGE", p.Token);
            };
            System.Diagnostics.Debug.WriteLine($"TOKEN: {CrossFirebasePushNotification.Current.Token}");

            CrossFirebasePushNotification.Current.OnNotificationReceived += (s, p) =>
            {
                try
                {
                    System.Diagnostics.Debug.WriteLine("Received");
                    var mid = String.Empty;
                    foreach (var data in p.Data)
                    {
                        System.Diagnostics.Debug.WriteLine($"{data.Key} : {data.Value}");
                        if (data.Key.Contains("mid"))
                        {
                            mid = data.Value.ToString();
                            break;
                        }
                    }


                    System.Diagnostics.Debug.WriteLine("Message Id: " + mid);

                    if (AppSettings.HasRegistered)
                    {
                        AppSettings.Mid = mid;
                        MessagingCenter.Send<AppBrowserAction, string>(new AppBrowserAction(), "Action", mid);
                    }
                }
                catch (Exception ex)
                {

                }

            };

            CrossFirebasePushNotification.Current.OnNotificationOpened += (s, p) =>
            {
                try
                {
                    System.Diagnostics.Debug.WriteLine("Opened");
                    Mvx.Resolve<IPlatformService>().SetBadge(0);
                    var mid = String.Empty;
                    foreach (var data in p.Data)
                    {
                        System.Diagnostics.Debug.WriteLine($"{data.Key} : {data.Value}");
                        if (data.Key.Contains("mid"))
                        {
                            mid = data.Value.ToString();
                            break;
                        }
                    }


                    System.Diagnostics.Debug.WriteLine("Message Id: " + mid);

                    if (AppSettings.HasRegistered)
                    {
                        AppSettings.Mid = mid;
                        MessagingCenter.Send<AppBrowserAction, string>(new AppBrowserAction(), "Action", mid);
                    }
                }
                catch
                {

                }

            };
            CrossFirebasePushNotification.Current.OnNotificationDeleted += (s, p) =>
            {
                System.Diagnostics.Debug.WriteLine("Dismissed");
            };

            CrossFirebasePushNotification.Current.OnNotificationError += (source, e) =>
            {
                System.Diagnostics.Debug.WriteLine("Error");
            };

        }

        protected async override void OnSleep()
        {
            // Handle when your app sleeps
            AppSettings.Mid = String.Empty;
        }

        protected async override void OnResume()
        {
            System.Diagnostics.Debug.WriteLine("Dismissed");
        }

    }
}
