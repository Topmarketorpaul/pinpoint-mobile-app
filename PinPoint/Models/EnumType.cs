﻿using System;
namespace PinPoint.Models
{
    public enum RegisterResult
    {
        Success = 1,
        LoginFailure = 2,
        MissingData = 3,
        CouldNotParseGeoLocation = 4,
        NotOnCampus = 5,
        NotAssociatedToAnyServers = 6,
        NotAssociatedToAnyCampuses = 7,
        NoServerConfiguredForGeo = 8,
        NoMatchOnIp = 9
    }

}
