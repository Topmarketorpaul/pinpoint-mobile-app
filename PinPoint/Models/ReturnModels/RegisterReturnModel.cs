﻿using System;
namespace PinPoint.Models.ReturnModels
{
    public class RegisterReturnModel
    {
        public bool Success { get; set; }

        public string UserToken { get; set; }
    }
}
