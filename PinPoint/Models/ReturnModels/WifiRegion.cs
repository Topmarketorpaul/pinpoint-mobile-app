﻿using System;
namespace PinPoint.Models.ReturnModels
{
    public class WifiRegion
    {
        public int GeoFenceId { get; set; }

        public int TenantId { get; set; }

        public int CampusId { get; set; }

        public bool Inbound { get; set; }

        public double Latitude { get; set; }

        public double Longitude { get; set; }

        public float Radius { get; set; }

        public string Ssid { get; set; }

        public string PreSharedKey { get; set; }
    }
}
