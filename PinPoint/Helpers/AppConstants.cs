﻿using System;
namespace PinPoint.Helpers
{
    public class AppConstants
    {
        public const string APP_URL = "https://mobile.vuteur.com";

        public const string LOGIN_URL = "https://mobile.vuteur.com/Account/Login";

        public const string LOGOUT_URL = "https://mobile.vuteur.com/Account/logout";
    }
}
