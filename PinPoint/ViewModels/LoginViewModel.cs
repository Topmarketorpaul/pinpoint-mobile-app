﻿using System;
using System.Threading.Tasks;
using MvvmCross.Core.Navigation;
using MvvmCross.Core.ViewModels;
using PinPoint.Services;
using PinPoint.ViewModels.Base;
using PinPoint.Helpers;
using Xamarin.Essentials;
using PinPoint.CommandBindings;
using PinPoint.Models;
using System.Diagnostics;
using Newtonsoft.Json;

namespace PinPoint.ViewModels
{
    public class LoginViewModel : BaseViewModel
    {
        private readonly IApiService ApiService;

        private readonly IPlatformService PlatformService;

        #region Constructor

        public LoginViewModel(IMvxNavigationService navigationService, IDialogService dialogService,
                              IApiService apiService, IPlatformService platformService) : base(navigationService, dialogService)
        {
            this.ApiService = apiService;
            this.PlatformService = platformService;
        }

        #endregion

        #region Overrides

        public override async void ViewAppeared()
        {
            if (!AppSettings.HasRegistered)
            {
                await CheckPermission();
            }

            LoadData();
            base.ViewAppeared();
        }

        public override Task Initialize()
        {

            return base.Initialize();
        }

        #endregion

        #region Properties

        #region TenancyName

        private string mTenancyName;
        public string TenancyName
        {
            get
            {
                return mTenancyName;
            }
            set
            {
                SetProperty(ref mTenancyName, value);
            }
        }

        #endregion

        #region UserName

        private string mUserName;
        public string UserName
        {
            get
            {
                return mUserName;
            }
            set
            {
                SetProperty(ref mUserName, value);
            }
        }

        #endregion

        #region Password

        private string mPassword;
        public string Password
        {
            get
            {
                return mPassword;
            }
            set
            {
                SetProperty(ref mPassword, value);
            }
        }

        #endregion

        #region IsRegistering

        private bool mIsRegistering;
        public bool IsRegistering
        {
            get
            {
                return mIsRegistering;
            }
            set
            {
                SetProperty(ref mIsRegistering, value);
            }
        }

        #endregion

        #region IsProcessing

        private bool mIsProcessing;
        public bool IsProcessing
        {
            get
            {
                return mIsProcessing;
            }
            set
            {
                SetProperty(ref mIsProcessing, value);
            }
        }

        #endregion

        #endregion

        #region Commands

        public IMvxAsyncCommand LoginCommand => new MvxAsyncCommand(Login);

        private async Task Login()
        {
            try
            {
                ShowProcess();

#if DEBUG
                this.TenancyName = "vuteur";
                this.UserName = "paul";
                this.Password = "t0pguru";
#endif
                if (String.IsNullOrEmpty(this.TenancyName))
                {
                    await DisplayAlert("", "Please enter Campus Key", "OK");
                    return;
                }

                if (String.IsNullOrEmpty(this.UserName))
                {
                    await DisplayAlert("", "Please enter Username", "OK");
                    return;
                }

                if (String.IsNullOrEmpty(this.Password))
                {
                    await DisplayAlert("", "Please enter Password", "OK");
                    return;
                }


                var result = await this.ApiService.Login(this.TenancyName, this.UserName, this.Password);

                if (result.IsSuccess)
                {
                    if (result.ResponseObject.Error != null)
                    {
                        await DisplayAlert("", result.ResponseObject.Error.Message, "OK");
                        return;
                    }
                    else
                    {
                        if (!AppSettings.HasRegistered)
                        {
                            await RegisterProcess();
                        }

                        //AppSettings.HasRegistered = true;


                        AppSettings.UserName = this.UserName;
                        AppSettings.Password = this.Password;
                        AppSettings.TenancyName = this.TenancyName;
                        await ClearStackAndNavigateToPage<AppBrowserViewModel, string>($"https://mobile.vuteur.com/Account/LoginMobile?username={AppSettings.UserName}&password={AppSettings.Password}&tenancyName={AppSettings.TenancyName}");

                        //if (AppSettings.HasRegistered)
                        //{
                        //    AppSettings.UserName = this.UserName;
                        //    AppSettings.Password = this.Password;
                        //    AppSettings.TenancyName = this.TenancyName;
                        //    await ClearStackAndNavigateToPage<AppBrowserViewModel, string>($"https://mobile.vuteur.com/Account/LoginMobile?username={AppSettings.UserName}&password={AppSettings.Password}&tenancyName={AppSettings.TenancyName}");
                        //    //await ClearStackAndNavigateToPage<AppBrowserViewModel, string>($"https://mobile.vuteur.com/Account/Login?username={AppSettings.UserName}&password={AppSettings.Password}&tenancyName={AppSettings.TenancyName}");
                        //}
                        //else
                        //{
                        //    AppSettings.UserName = String.Empty;
                        //    AppSettings.Password = String.Empty;
                        //    AppSettings.TenancyName = String.Empty;
                        //}

                    }
                }
                else
                {
                    await DisplayAlert("", "Unable to login at this time", "OK");
                }
            }
            catch
            {
                await DisplayAlert("", "Unable to login at this time", "OK");
            }
            finally
            {
                HideProcess();
            }
        }

        public IMvxAsyncCommand ForgotPasswordCommand => new MvxAsyncCommand(ForgotPassword);

        private async Task ForgotPassword()
        {
            await this.NavigationService.Navigate<AppBrowserViewModel, string>("https://mobile.vuteur.com/Account/ForgotPassword");
        }

        #endregion

        #region Methods

        async Task RegisterProcess()
        {
            try
            {
                var ip = this.PlatformService.GetIPAddress();

                var geo = await GetLocation();

                var deviceId = PlatformService.GetUUID();

                var dev = GetDeviceInfo();

                var ver = DeviceInfo.VersionString;

                var result = await this.ApiService.Register(new RegisterBindingModel()
                {
                    TenancyName = this.TenancyName,
                    UserName = this.UserName,
                    Password = this.Password,
                    Ip = ip,
                    Geo = geo,
                    Mac = deviceId,
                    Dev = dev,
                    Ver = ver
                });


                if (result.IsSuccess)
                {
                    Debug.WriteLine("REGISTER: " + result.ResponseObject.ToString());
                    AppSettings.HasRegistered = result.ResponseObject == RegisterResult.Success;
                    this.PlatformService.Log("REGISTER", JsonConvert.SerializeObject(new
                    {
                        Request = new
                        {

                            TenancyName = this.TenancyName,
                            UserName = this.UserName,
                            Password = this.Password,
                            Ip = ip,
                            Geo = geo,
                            Mac = deviceId,
                            Dev = dev,
                            Ver = ver
                        },
                        Response = result.ResponseObject.ToString()
                    }));
                    //if (result.ResponseObject != RegisterResult.Success)
                    //{
                    //    await DisplayAlert("", GetMessage(result.ResponseObject), "OK");
                    //}
                }
                else
                {
                    //await DisplayAlert("", "Unable to register at this time", "OK");
                }
            }
            catch
            {
                //await DisplayAlert("", "Unable to register at this time", "OK");
            }
        }

        async Task<string> GetLocation()
        {

            string result = String.Empty;
            try
            {
                var request = new GeolocationRequest(GeolocationAccuracy.Medium, TimeSpan.FromSeconds(15));
                var location = await Geolocation.GetLocationAsync(request);

                if (location != null)
                {
                    result = $"{location.Latitude}|{location.Longitude}";
                }
            }
            catch (FeatureNotSupportedException)
            {
                // Handle not supported on device exception

                await DisplayAlert("", "Device does not support location", "OK");
            }
            catch (PermissionException)
            {
                // Handle permission exception
                await DisplayAlert("", "Please enable location on this device to use all the features of this application.", "OK");
            }
            catch (Exception)
            {
                await DisplayAlert("", "Unable to get location", "OK");
            }
            return result;
        }

        string GetDeviceInfo()
        {
            var result = "Unknown";

            if (DeviceInfo.Platform == DeviceInfo.Platforms.Android)
            {
                if (DeviceInfo.Idiom == DeviceInfo.Idioms.Tablet)
                {
                    result = "Android Tablet";
                }

                if (DeviceInfo.Idiom == DeviceInfo.Idioms.Phone)
                {
                    result = "Android Phone";
                }
            }
            else
            {
                if (DeviceInfo.Idiom == DeviceInfo.Idioms.Tablet)
                {
                    result = "Apple Tablet";
                }

                if (DeviceInfo.Idiom == DeviceInfo.Idioms.Phone)
                {
                    result = "Apple Phone";
                }
            }

            return result;
        }

        void ShowProcess()
        {
            if (!AppSettings.HasRegistered)
            {
                IsRegistering = true;
            }
            else
            {
                IsProcessing = true;
            }
        }

        void HideProcess()
        {
            IsRegistering = false;
            IsProcessing = false;
        }

        async void LoadData()
        {
            if (AppSettings.HasRegistered)
            {
                ShowProcess();
                string mid = !String.IsNullOrEmpty(AppSettings.Mid) ? "&mid=" + AppSettings.Mid : String.Empty;
                AppSettings.Mid = String.Empty;
                var url = $"https://mobile.vuteur.com/Account/LoginMobile?username={AppSettings.UserName}&password={AppSettings.Password}&tenancyName={AppSettings.TenancyName}{mid}";
                await ClearStackAndNavigateToPage<AppBrowserViewModel, string>(url);
                HideProcess();
            }
        }
        string GetMessage(RegisterResult result)
        {
            return "Please connect to your campus network and then launch the VuTeur application to use all the available features.";

            //switch (result)
            //{
            //    case RegisterResult.CouldNotParseGeoLocation:
            //        return "Could not parse geo location";
            //    case RegisterResult.LoginFailure:
            //        return "Login failure";
            //    case RegisterResult.MissingData:
            //        return "Missing Data";
            //    case RegisterResult.NoMatchOnIp:
            //        return "No match on ip";
            //    case RegisterResult.NoServerConfiguredForGeo:
            //        return "No server configured for geo";
            //    case RegisterResult.NotAssociatedToAnyCampuses:
            //        return "Not associated to any campuses";
            //    case RegisterResult.NotAssociatedToAnyServers:
            //        return "Not associated to any servers";
            //    case RegisterResult.NotOnCampus:
            //        return "Not on campus";
            //    default:
            //        return "Success";
            //}
        }

        public async Task CheckPermission()
        {
            if (!PlatformService.LocationServicesEnabled)
            {
                await DisplayAlert("Location Required", "Please enable location on this device to use all the features of this application.", "Got it");
                if (AppSettings.IsFirstTime)
                {
                    PlatformService.RequestPermission();
                    AppSettings.IsFirstTime = false;
                }
                else
                {
                    PlatformService.RequestPermissionSettings();
                }
                //return false;
            }

            //return true;
        }
        #endregion
    }
}
