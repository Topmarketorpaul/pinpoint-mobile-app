﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MvvmCross.Core.Navigation;
using MvvmCross.Core.ViewModels;
using MvvmCross.Core.ViewModels.Hints;
using PinPoint.Helpers;
using PinPoint.Infrastructure;
using PinPoint.Presenter;
using PinPoint.Services;

namespace PinPoint.ViewModels.Base
{
    public abstract class BaseWithObjectViewModel<TParam> : MvxViewModel<TParam>
    {
        protected readonly IMvxNavigationService NavigationService;

        protected readonly IDialogService DialogService;

        public BaseWithObjectViewModel(IMvxNavigationService navigationService, IDialogService dialogService)
        {
            this.NavigationService = navigationService;
            this.DialogService = dialogService;
        }


        #region Properties

        #region IsLoading

        private bool mIsLoading;
        public bool IsLoading
        {
            get
            {
                return mIsLoading;
            }
            set
            {
                SetProperty(ref mIsLoading, value);
            }
        }

        #endregion

        #endregion

        #region Methods

        protected async Task DisplayAlert(string title, string message, string canncel)
        {
            await DialogService?.ShowMessage(title, message, canncel);
        }

        protected async Task<bool> DisplayAlert(string title, string message, string positiveText, string negativeText)
        {
            return await DialogService?.ShowMessage(title, message, buttonConfirmText: positiveText, buttonCancelText: negativeText);
        }

        protected async Task<string> ShowMultipleSelection(string title, string[] options)
        {
            return await DialogService?.ShowMultipleSelection(title, options);
        }

        protected async Task DisplayErrors(List<string> errors)
        {
            await DialogService?.ShowMessage("", String.Join(Environment.NewLine, errors), "OK");
        }

        protected bool CheckIfNeedToSignin(ApiResponse response)
        {
            if (response.ResponseStatusCode == 401)
            {
                Logout();
                return true;
            }

            return false;
        }

        protected async void Logout()
        {
            // Clear saved data


            // Navigate to login page
            await this.ClearStackAndNavigateToPage<LoginViewModel>();
        }

        public void ShowLoading()
        {
            this.IsLoading = true;
        }

        public void HideLoading()
        {
            this.IsLoading = false;
        }

        #region Navigations

        protected void PopToPage<TViewModel>() where TViewModel : MvxViewModel
        {
            var hint = new MvxPopPresentationHint(typeof(TViewModel));
            this.NavigationService.ChangePresentation(hint);
        }


        public async Task ClearStackAndNavigateToPage<TViewModel>() where TViewModel : MvxViewModel
        {
            var presentation = new MvxBundle(new Dictionary<string, string> { { PresentationConstantValue.CLEAR_STACK_AND_SHOW_PAGE, "" } });

            await this.NavigationService.Navigate<TViewModel>(presentationBundle: presentation);
        }
        public async Task ClearStackAndNavigateToPage<TViewModel, TParameter>(TParameter param) where TViewModel : MvxViewModel<TParameter>
        {
            var presentation = new MvxBundle(new Dictionary<string, string> { { PresentationConstantValue.CLEAR_STACK_AND_SHOW_PAGE, "" } });

            await this.NavigationService.Navigate<TViewModel, TParameter>(param, presentationBundle: presentation);
        }


        #endregion


        #endregion

        #region Commands

        #region CloseCommand

        public IMvxAsyncCommand CloseCommand => new MvxAsyncCommand(ClosePage);

        protected async Task ClosePage()
        {
            await this.NavigationService.Close(this);
        }


        #endregion

        #endregion
    }
}
