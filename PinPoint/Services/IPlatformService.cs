﻿using System;
namespace PinPoint.Services
{
    public interface IPlatformService
    {
        string GetIPAddress();

        string GetUUID();

        void SetBadge(int count);

        void Log(string tag, string data);

        bool LocationServicesEnabled { get; }

        void RequestPermission();

        void RequestPermissionSettings();
    }
}
