﻿using System;
using System.Threading.Tasks;
using PinPoint.Helpers;
using PinPoint.Models.ReturnModels;
using Flurl.Http;
using System.Collections.Generic;
using Newtonsoft.Json;
using PinPoint.Services.Base;
using PinPoint.Infrastructure;
using PinPoint.CommandResutls;
using PinPoint.CommandBindings;
using PinPoint.Models;

namespace PinPoint.Services
{
    public class ApiService : BaseService, IApiService
    {
        const string LOGIN_URL = AppConstants.APP_URL + "/account/login";

        const string REGISTER_URL = AppConstants.APP_URL + "/Account/RegisterDevice";

        const string SET_FIREBASE_TOKEN_URL = AppConstants.APP_URL + "/pushnotification/settoken";

        public async Task<ApiResponse<LoginResult>> Login(string tenancyName, string usernameOrEmailAddress, string password)
        {
            var body = new
            {
                TenancyName = tenancyName,
                UsernameOrEmailAddress = usernameOrEmailAddress,
                Password = password
            };

            return await DoPostForm<LoginResult>(LOGIN_URL, body, false);
        }

        public async Task<ApiResponse<RegisterResult>> Register(RegisterBindingModel model)
        {
            return await DoPostForm<RegisterResult>(REGISTER_URL, model, false);
        }

        public async Task<ApiResponse<FirebaseTokenResult>> SetToken(string tenancyName, string usernameOrEmailAddress, string password, string token)
        {
            var body = new
            {
                Tenant = tenancyName,
                UserName = usernameOrEmailAddress,
                Password = password,
                Token = token
            };

            return await DoPostForm<FirebaseTokenResult>(SET_FIREBASE_TOKEN_URL, body, false);
        }
    }

    public interface IApiService
    {
        Task<ApiResponse<LoginResult>> Login(string tenancyName, string usernameOrEmailAddress, string password);

        Task<ApiResponse<RegisterResult>> Register(RegisterBindingModel model);

        Task<ApiResponse<FirebaseTokenResult>> SetToken(string tenancyName, string usernameOrEmailAddress, string password, string token);
    }
}
