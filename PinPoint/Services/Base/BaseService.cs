﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Flurl.Http;
using PinPoint.Helpers;
using PinPoint.Infrastructure;

namespace PinPoint.Services.Base
{
    public class BaseService
    {
        #region Methods

        protected async Task<ApiResponse<T>> DoGetAll<T>(string url, bool isAuthorized = true) where T : class, new()
        {
            ApiResponse<T> apiResponse = new ApiResponse<T>();
            T result;
            try
            {
                string token = AppSettings.AccessToken;
                if (isAuthorized)
                {
                    result = await url.WithOAuthBearerToken(token).GetJsonAsync<T>();
                }
                else
                {
                    result = await url.GetJsonAsync<T>();
                }
                apiResponse.IsSuccess = true;
                apiResponse.ResponseStatusCode = 200;
                apiResponse.ResponseObject = result;
            }
            catch (FlurlHttpException fhx)
            {
                apiResponse = new ApiResponse<T>()
                {
                    IsSuccess = false,
                    Errors = new List<string>() { fhx.Message },
                    ResponseStatusCode = fhx.Call.HttpStatus.HasValue ? (int)fhx.Call.HttpStatus.Value : 500
                };
            }
            catch (Exception ex)
            {
                apiResponse = new ApiResponse<T>()
                {
                    IsSuccess = false,
                    Errors = new List<string>() { ex.Message },
                    ResponseStatusCode = 500
                };
            }
            return apiResponse;
        }

        protected async Task<ApiResponse<T>> Post<T>(string url, object body, bool isAuthorized = true) where T : class, new()
        {
            ApiResponse<T> apiResponse = new ApiResponse<T>();
            T result;
            try
            {
                string token = AppSettings.AccessToken;
                if (isAuthorized)
                {
                    result = await url.WithOAuthBearerToken(token).PostJsonAsync(body).ReceiveJson<T>();
                }
                else
                {
                    result = await url.PostJsonAsync(body).ReceiveJson<T>();
                }
                apiResponse.IsSuccess = true;
                apiResponse.ResponseStatusCode = 200;
                apiResponse.ResponseObject = result;
            }
            catch (FlurlHttpException fhx)
            {
                apiResponse = new ApiResponse<T>()
                {
                    IsSuccess = false,
                    Errors = new List<string>() { fhx.Message },
                    ResponseStatusCode = fhx.Call.HttpStatus.HasValue ? (int)fhx.Call.HttpStatus.Value : 500
                };
            }
            catch (Exception ex)
            {
                apiResponse = new ApiResponse<T>()
                {
                    IsSuccess = false,
                    Errors = new List<string>() { ex.Message },
                    ResponseStatusCode = 500
                };
            }
            return apiResponse;
        }

        protected async Task<ApiResponse<T>> DoPostForm<T>(string url, object body, bool isAuthorized = true)
        {
            ApiResponse<T> apiResponse = new ApiResponse<T>();
            T result;
            try
            {
                string token = AppSettings.AccessToken;
                if (isAuthorized)
                {
                    result = await url.WithOAuthBearerToken(token).PostUrlEncodedAsync(body).ReceiveJson<T>();
                }
                else
                {
                    result = await url.PostUrlEncodedAsync(body).ReceiveJson<T>();
                }
                apiResponse.IsSuccess = true;
                apiResponse.ResponseStatusCode = 200;
                apiResponse.ResponseObject = result;
            }
            catch (FlurlHttpException fhx)
            {
                apiResponse = new ApiResponse<T>()
                {
                    IsSuccess = false,
                    Errors = new List<string>() { fhx.Message },
                    ResponseStatusCode = fhx.Call.HttpStatus.HasValue ? (int)fhx.Call.HttpStatus.Value : 500
                };
            }
            catch (Exception ex)
            {
                apiResponse = new ApiResponse<T>()
                {
                    IsSuccess = false,
                    Errors = new List<string>() { ex.Message },
                    ResponseStatusCode = 500
                };
            }
            return apiResponse;
        }

        protected async Task<ApiResponse<T>> Get<T>(string url, bool isAuthorized = true) where T : class, new()
        {
            ApiResponse<T> apiResponse = new ApiResponse<T>();
            T result;
            try
            {
                string token = AppSettings.AccessToken;
                if (isAuthorized)
                {
                    result = await url.WithOAuthBearerToken(token).GetJsonAsync<T>();
                }
                else
                {
                    result = await url.GetJsonAsync<T>();
                }
                apiResponse.IsSuccess = true;
                apiResponse.ResponseStatusCode = 200;
                apiResponse.ResponseObject = result;
            }
            catch (FlurlHttpException fhx)
            {
                apiResponse = new ApiResponse<T>()
                {
                    IsSuccess = false,
                    Errors = new List<string>() { fhx.Message },
                    ResponseStatusCode = fhx.Call.HttpStatus.HasValue ? (int)fhx.Call.HttpStatus.Value : 500
                };
            }
            catch (Exception ex)
            {
                apiResponse = new ApiResponse<T>()
                {
                    IsSuccess = false,
                    Errors = new List<string>() { ex.Message },
                    ResponseStatusCode = 500
                };
            }
            return apiResponse;
        }
        #endregion
    }
}
