﻿using System;
namespace PinPoint.CommandResutls
{
    public class LoginResult
    {
        public string TargetUrl { get; set; }

        public bool Success { get; set; }

        public Error Error { get; set; }
    }

    public class Error
    {
        public int Code { get; set; }

        public string Message { get; set; }
    }
}
