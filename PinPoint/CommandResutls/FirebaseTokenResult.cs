﻿using System;
namespace PinPoint.CommandResutls
{
    public class FirebaseTokenResult
    {
        public string TargetUrl { get; set; }

        public bool Success { get; set; }

        public Error Error { get; set; }
    }
}
