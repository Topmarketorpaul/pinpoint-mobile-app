﻿using System;
namespace PinPoint.CommandBindings
{
    public class RegisterBindingModel
    {
        public string TenancyName { get; set; }

        public string UserName { get; set; }

        public string Password { get; set; }

        public string Ip { get; set; }

        public string Geo { get; set; }

        public string Mac { get; set; }

        public string Dev { get; set; }

        public string Ver { get; set; }
    }
}
