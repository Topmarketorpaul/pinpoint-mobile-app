﻿using System;
using Xamarin.Forms;

namespace PinPoint.Controls
{
    public interface IHybridWebViewAction
    {
        void GoBack();
        bool CanGoBack();
        void Refresh();
        void LoadRequest(string url);
    }

    public class HybridWebView : View
    {
        public IHybridWebViewAction HybridWebViewAction { get; set; }
        public Action<string> Navigating;
        public static readonly BindableProperty UriProperty = BindableProperty.Create(
          propertyName: "Uri",
          returnType: typeof(string),
          declaringType: typeof(HybridWebView),
          defaultValue: default(string));

        public string Uri
        {
            get { return (string)GetValue(UriProperty); }
            set { SetValue(UriProperty, value); }
        }

        public void CleanUp()
        {
            HybridWebViewAction = null;
            Navigating = null;
        }
    }
}
