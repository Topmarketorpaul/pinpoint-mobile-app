﻿using System;
using System.Collections.Generic;
using System.Net;
using Android;
using Android.Content;
using Android.Content.PM;
using Android.Support.Design.Widget;
using Android.Support.V4.App;
using Android.Support.V4.Content;
using HockeyApp;
using PinPoint.Services;
using Plugin.CurrentActivity;
using Android.Provider;

namespace PinPoint.Droid.Services
{
    public class PlatformService : IPlatformService
    {
        private MainActivity Activity => CrossCurrentActivity.Current.Activity as MainActivity;

        public bool LocationServicesEnabled => CheckPermissions();

        public string GetIPAddress()
        {
            IPAddress[] adresses = Dns.GetHostAddresses(Dns.GetHostName());

            if (adresses != null && adresses[0] != null)
            {
                return adresses[0].ToString();
            }
            else
            {
                return String.Empty;
            }
        }

        public string GetUUID()
        {
            return Android.OS.Build.Serial;
        }

        public void Log(string tag, string data)
        {
            MetricsManager.TrackEvent(tag, new Dictionary<string, string> { { GetUUID(), data } },
                                      new Dictionary<string, double> { { "time", 1.0 } });
        }

        public void RequestPermission()
        {
            RequestPermission();
        }

        public void RequestPermissionSettings()
        {
            Intent intent = new Intent();
            intent.SetAction(Settings.ActionApplicationDetailsSettings);
            Android.Net.Uri uri = Android.Net.Uri.FromParts("package", Activity.PackageName, null);
            intent.SetData(uri);
            Activity.StartActivity(intent);
        }

        public void SetBadge(int count)
        {

        }

        private void RequestPermissions()
        {
            var shouldProvideRationale = ActivityCompat.ShouldShowRequestPermissionRationale(this.Activity, Manifest.Permission.AccessFineLocation);

            if (shouldProvideRationale)
            {
                //Log.Info(typeof(MainActivity).Name, "Displaying permission rationale to provide additional context.");
                // var listener = (View.IOnClickListener)new RequestPermissionsClickListener { Activity = this.Activity };
                ShowSnackbar("Please enable location on this device to use all the features of this application.", Android.Resource.String.Ok);
            }
            else
            {
                ActivityCompat.RequestPermissions(this.Activity, new[] { Manifest.Permission.AccessFineLocation }, MainActivity.REQUEST_PERMISSIONS_REQUEST_CODE);
            }


        }

        private bool CheckPermissions()
        {
            var permissionState = ContextCompat.CheckSelfPermission(CrossCurrentActivity.Current.Activity, Manifest.Permission.AccessFineLocation);
            return permissionState == (int)Permission.Granted;
        }

        private void ShowSnackbar(string mainText, int actionStringId)
        {
            Snackbar.Make(this.Activity.FindViewById(Android.Resource.Id.Content),
                          mainText,
                    Snackbar.LengthIndefinite)
                    .SetAction(this.Activity.GetString(actionStringId), (obj) =>
                    {
                        ActivityCompat.RequestPermissions(Activity, new[] { Manifest.Permission.AccessFineLocation }, MainActivity.REQUEST_PERMISSIONS_REQUEST_CODE);
                    }).Show();
        }
    }
}
