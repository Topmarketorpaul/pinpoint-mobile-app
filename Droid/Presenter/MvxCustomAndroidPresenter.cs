﻿using System;
using System.Collections.Generic;
using System.Reflection;
using MvvmCross.Forms.Droid.Views;
using MvvmCross.Forms.Platform;
using MvvmCross.Forms.Views;
using MvvmCross.Platform;
using PinPoint.Presenter;

namespace PinPoint.Droid.Presenter
{
    public class MvxCustomAndroidPresenter : MvxFormsAndroidViewPresenter
    {
        #region Constructor

        public MvxCustomAndroidPresenter(IEnumerable<Assembly> androidViewAssemblies,
                                        MvxFormsApplication formsApplication) : base(androidViewAssemblies, formsApplication)
        {
        }

        #endregion

        private IMvxFormsPagePresenter mFormsPagePresenter;
        public override MvvmCross.Forms.Views.IMvxFormsPagePresenter FormsPagePresenter
        {
            get
            {
                if (mFormsPagePresenter == null)
                {
                    mFormsPagePresenter = new MvxFormCustomPresenter(this.FormsApplication);
                    Mvx.RegisterSingleton(mFormsPagePresenter);
                }
                return mFormsPagePresenter;
            }
            set
            {
                mFormsPagePresenter = value;
            }
        }
    }
}
