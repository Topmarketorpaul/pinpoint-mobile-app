﻿using System;
using Android.App;
using Android.Content;
using Android.Content.PM;
using MvvmCross.Droid.Views;

namespace PinPoint.Droid
{
    [Activity(
        Label = "VuTeur"
        , MainLauncher = true
        , Icon = "@drawable/app_icon"
        , Theme = "@style/Theme.Splash"
        , ScreenOrientation = ScreenOrientation.Portrait)]
    public class SplashScreen : MvxSplashScreenActivity
    {
        public SplashScreen()
            : base(Resource.Layout.SplashScreen)
        {
        }

        protected override void TriggerFirstNavigate()
        {
            StartActivity(typeof(MainActivity));
            //var mainIntent = new Intent(Application.Context, typeof(MainActivity));

            //if (Intent.Extras != null)
            //{
            //  mainIntent.PutExtras(Intent.Extras);
            //}
            //mainIntent.SetFlags(ActivityFlags.SingleTop);
            base.TriggerFirstNavigate();

            Finish();
        }
    }
}
