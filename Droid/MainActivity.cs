﻿using System;

using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using MvvmCross.Forms.Droid.Views;
using HockeyApp.Android;
using Plugin.FirebasePushNotification;
using HockeyApp.Android.Metrics;

namespace PinPoint.Droid
{
    [Activity(Label = "PinPoint Fire", Icon = "@drawable/app_icon", Theme = "@style/MainTheme", ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class MainActivity : MvxFormsAppCompatActivity
    {
        public const int REQUEST_PERMISSIONS_REQUEST_CODE = 34;

        protected override void OnCreate(Bundle bundle)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            base.OnCreate(bundle);

            //Window.AddFlags(WindowManagerFlags.TranslucentNavigation);
            //Window.AddFlags(WindowManagerFlags.TranslucentStatus);

            Xamarin.Essentials.Platform.Init(this, bundle);


            FirebasePushNotificationManager.ProcessIntent(this, Intent);

            CrashManager.Register(this, "bdcdaccaeefe476eabdf38d2d3f75016");
            MetricsManager.Register(this.Application, "bdcdaccaeefe476eabdf38d2d3f75016");
            MetricsManager.EnableUserMetrics();
        }

        protected override void OnResume()
        {
            base.OnResume();

        }

        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Permission[] grantResults)
        {
            Xamarin.Essentials.Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);

            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }

        protected override void OnNewIntent(Intent intent)
        {
            base.OnNewIntent(intent);
            FirebasePushNotificationManager.ProcessIntent(this, intent);
        }
    }
}
