﻿using System;
using Android.App;
using Android.OS;
using Android.Runtime;
using HockeyApp.Android;
using HockeyApp.Android.Metrics;
using Plugin.CurrentActivity;
using Plugin.FirebasePushNotification;

namespace PinPoint.Droid
{
#if DEBUG
    [Application(Debuggable = true)]
#else
    [Application(Debuggable = false)]
#endif
    public class MainApplication : Application
    {
        public MainApplication(IntPtr handle, JniHandleOwnership transer)
          : base(handle, transer)
        {
        }

        public override void OnCreate()
        {
            base.OnCreate();
            CrossCurrentActivity.Current.Init(this);
            if (Build.VERSION.SdkInt >= Android.OS.BuildVersionCodes.O)
            {
                //Change for your default notification channel id here
                FirebasePushNotificationManager.DefaultNotificationChannelId = "DefaultChannel";

                //Change for your default notification channel name here
                FirebasePushNotificationManager.DefaultNotificationChannelName = "General";
            }


            //If debug you should reset the token each time.
#if DEBUG
            FirebasePushNotificationManager.Initialize(this, true);
#else
              FirebasePushNotificationManager.Initialize(this,false);
#endif

            //CrashManager.Register(this, "bdcdaccaeefe476eabdf38d2d3f75016");
            //MetricsManager.Register(this, "bdcdaccaeefe476eabdf38d2d3f75016");
            //MetricsManager.EnableUserMetrics();
        }
    }
}
