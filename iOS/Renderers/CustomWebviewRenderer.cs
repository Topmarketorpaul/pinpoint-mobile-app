﻿using System;
using System.Diagnostics;
using Foundation;
using PinPoint.Controls;
using PinPoint.iOS.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(CustomWebView), typeof(CustomWebviewRenderer))]

namespace PinPoint.iOS.Renderers
{
    public class CustomWebviewRenderer : WebViewRenderer
    {
        public CustomWebviewRenderer()
        {
        }

        public override void LoadRequest(NSUrlRequest r)
        {

            try
            {
                // reconstruct original URL
                var decodedStringUrl = new NSString(r.Url.AbsoluteString).CreateStringByReplacingPercentEscapes(NSStringEncoding.UTF8);

                // call load request with original string
                base.LoadRequest(new NSUrlRequest(new NSUrl(decodedStringUrl)));
            }
            catch (Exception ex)
            {
                base.LoadRequest(r);
            }


        }

        protected override void OnElementChanged(VisualElementChangedEventArgs e)
        {
            base.OnElementChanged(e);
        }

    }
}
