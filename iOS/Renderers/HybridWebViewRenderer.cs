﻿using System;
using System.IO;
using Foundation;
using PinPoint.Controls;
using WebKit;
using Xamarin.Forms.Platform.iOS;
using System.Diagnostics;
using PinPoint.iOS.Renderers;
using Xamarin.Forms;
using ObjCRuntime;

[assembly: ExportRenderer(typeof(HybridWebView), typeof(HybridWebViewRenderer))]
namespace PinPoint.iOS.Renderers
{
    public class HybridWebViewRenderer : ViewRenderer<HybridWebView, WKWebView>, IHybridWebViewAction
    {
        WKUserContentController userController;

        protected override void OnElementChanged(ElementChangedEventArgs<HybridWebView> e)
        {
            base.OnElementChanged(e);

            if (Control == null)
            {
                userController = new WKUserContentController();
                //var script = new WKUserScript(new NSString(JavaScriptFunction), WKUserScriptInjectionTime.AtDocumentEnd, false);
                //userController.AddUserScript(script);
                //userController.AddScriptMessageHandler(this, "invokeAction");


                var config = new WKWebViewConfiguration { UserContentController = userController };
                config.Preferences.JavaScriptEnabled = true;
                config.Preferences.JavaScriptCanOpenWindowsAutomatically = true;
                var webView = new WKWebView(Frame, config);
                webView.NavigationDelegate = new HybridWKWebViewDelegate(this);

                webView.CustomUserAgent = @"Mozilla/5.0 (iPhone; CPU iPhone OS 11_0 like Mac OS X) AppleWebKit/604.1.38 (KHTML, like Gecko) Version/11.0 Mobile/15A372 Safari/604.1";
                SetNativeControl(webView);
            }
            if (e.OldElement != null)
            {
                //userController.RemoveAllUserScripts();
                //userController.RemoveScriptMessageHandler("invokeAction");
                var hybridWebView = e.OldElement as HybridWebView;
                hybridWebView.CleanUp();
            }
            if (e.NewElement != null)
            {
                //string fileName = Path.Combine(NSBundle.MainBundle.BundlePath, string.Format("Content/{0}", Element.Uri));
                //Control.LoadRequest(new NSUrlRequest(new NSUrl(fileName, false)));
                e.NewElement.HybridWebViewAction = this;
                Control.LoadRequest(new NSUrlRequest(new NSUrl(Element.Uri)));

            }
        }

        public void GoBack()
        {
            Control.GoBack();
        }

        public bool CanGoBack()
        {
            return Control.CanGoBack;
        }

        public void Refresh()
        {
            Control.Reload();
        }

        public void LoadRequest(string url)
        {
            Control.LoadRequest(new NSUrlRequest(new NSUrl(url)));
        }
        public void Navigating(string url)
        {
            this.Element.Navigating?.Invoke(url);
        }
    }

    public class HybridWKWebViewDelegate : WKNavigationDelegate
    {
        HybridWebViewRenderer hybridWebViewRenderer;
        public HybridWKWebViewDelegate(HybridWebViewRenderer _webViewRenderer = null)
        {
            hybridWebViewRenderer = _webViewRenderer ?? new HybridWebViewRenderer();
        }

        [Export("webView:didStartProvisionalNavigation:")]
        public override void DidStartProvisionalNavigation(WKWebView webView, WKNavigation navigation)
        {
            hybridWebViewRenderer.Navigating(webView.Url.AbsoluteString);
        }

        [Export("webView:didFailProvisionalNavigation:")]
        public override void DidFailProvisionalNavigation(WKWebView webView, WKNavigation navigation, NSError error)
        {

        }

        [Export("webView:didFinishNavigation:")]
        public override void DidFinishNavigation(WKWebView webView, WKNavigation navigation)
        {

        }

        public override void DecidePolicy(WKWebView webView, WKNavigationAction navigationAction, Action<WKNavigationActionPolicy> decisionHandler)
        {
            switch (navigationAction.NavigationType)
            {
                case WKNavigationType.BackForward:
                    decisionHandler(WKNavigationActionPolicy.Allow);
                    break;
                case WKNavigationType.FormResubmitted:
                    decisionHandler(WKNavigationActionPolicy.Allow);
                    break;
                case WKNavigationType.FormSubmitted:
                    decisionHandler(WKNavigationActionPolicy.Allow);
                    break;
                case WKNavigationType.LinkActivated:
                    decisionHandler(WKNavigationActionPolicy.Allow);
                    break;
                case WKNavigationType.Other:
                    decisionHandler(WKNavigationActionPolicy.Allow);
                    break;
                case WKNavigationType.Reload:
                    decisionHandler(WKNavigationActionPolicy.Allow);
                    break;
            }
        }


        //[Export("webView:decidePolicyForNavigationAction:decisionHandler:")]
        //public void DecidePolicy(WKWebView webView, WKNavigationAction navigationAction, Action<WKNavigationActionPolicy> decisionHandler)
        //{

        //}
    }
}
