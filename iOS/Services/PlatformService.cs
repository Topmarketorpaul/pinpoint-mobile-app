﻿using System;
using System.Collections.Generic;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using CoreLocation;
using Foundation;
using HockeyApp;
using PinPoint.Services;
using UIKit;

namespace PinPoint.iOS.Services
{
    public class PlatformService : IPlatformService
    {
        protected CLLocationManager mLocMgr;


        public CLLocationManager LocMgr
        {
            get { return this.mLocMgr; }
        }

        public bool LocationServicesEnabled => CLLocationManager.Status == CLAuthorizationStatus.AuthorizedAlways;

        public string GetIPAddress()
        {
            String ipAddress = "";

            foreach (var netInterface in NetworkInterface.GetAllNetworkInterfaces())
            {
                if (netInterface.NetworkInterfaceType == NetworkInterfaceType.Wireless80211 ||
                    netInterface.NetworkInterfaceType == NetworkInterfaceType.Ethernet)
                {
                    foreach (var addrInfo in netInterface.GetIPProperties().UnicastAddresses)
                    {
                        if (addrInfo.Address.AddressFamily == AddressFamily.InterNetwork)
                        {
                            ipAddress = addrInfo.Address.ToString();

                        }
                    }
                }
            }

            return ipAddress;
        }

        public string GetUUID()
        {
            return UIKit.UIDevice.CurrentDevice.IdentifierForVendor.AsString();
        }

        public void Log(string tag, string data)
        {
            MetricsManager.TrackEvent(tag, new Dictionary<string, string> { { GetUUID(), data } },
                                       new Dictionary<string, double> { { "time", 1.0 } });
        }

        public void RequestPermission()
        {
            RequestLocationMonitor();
        }

        public void RequestPermissionSettings()
        {
            //UIApplication.SharedApplication.OpenUrl(new NSUrl("app-settings:"));
            RequestLocationMonitor();
        }

        void RequestLocationMonitor()
        {
            this.mLocMgr = new CLLocationManager();
            this.mLocMgr.PausesLocationUpdatesAutomatically = false;

            // iOS 8 has additional permissions requirements
            if (UIDevice.CurrentDevice.CheckSystemVersion(8, 0))
            {
                mLocMgr.RequestAlwaysAuthorization();
                mLocMgr.RequestWhenInUseAuthorization();
            }

            if (UIDevice.CurrentDevice.CheckSystemVersion(9, 0))
            {
                //mLocMgr.AllowsBackgroundLocationUpdates = true;
            }
        }

        public void SetBadge(int count)
        {
            UIApplication.SharedApplication.ApplicationIconBadgeNumber = count;
        }
    }
}
