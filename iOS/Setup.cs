﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using MvvmCross.Core.ViewModels;
using MvvmCross.Forms.iOS;
using MvvmCross.Forms.Platform;
using MvvmCross.Forms.Views;
using MvvmCross.iOS.Platform;
using MvvmCross.Platform;
using MvvmCross.Platform.Logging;
using MvvmCross.Platform.Platform;
using UIKit;
using PinPoint.iOS.Presenter;
using PinPoint.Services;
using PinPoint.iOS.Services;

namespace PinPoint.iOS
{
    public class Setup : MvxFormsIosSetup
    {
        public Setup(IMvxApplicationDelegate applicationDelegate, UIWindow window)
            : base(applicationDelegate, window)
        {
        }

        protected override MvxFormsApplication CreateFormsApplication()
        {
            return new App();
        }

        protected override MvxLogProviderType GetDefaultLogProviderType()
    => MvxLogProviderType.None;

        protected override IMvxApplication CreateApp()
        {
            return new CoreApp();
        }

        //protected override IMvxTrace CreateDebugTrace()
        //{
        //    return new DebugTrace();
        //}

        protected override IEnumerable<Assembly> GetViewAssemblies()
        {
            return new List<Assembly>(base.GetViewAssemblies().Union(new[] { typeof(App).GetTypeInfo().Assembly }));
        }

        protected override MvvmCross.iOS.Views.Presenters.IMvxIosViewPresenter CreatePresenter()
        {
            var presenter = new MvxCustomIosPresenter(this.ApplicationDelegate, this.Window, this.FormsApplication);
            Mvx.RegisterSingleton<IMvxFormsViewPresenter>(presenter);
            return presenter;
        }

        protected override void InitializeIoC()
        {
            base.InitializeIoC();
            Mvx.RegisterSingleton<IPlatformService>(new PlatformService());
        }
    }
}
