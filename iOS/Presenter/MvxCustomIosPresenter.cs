﻿using System;
using MvvmCross.Forms.iOS.Presenters;
using MvvmCross.Forms.Platform;
using MvvmCross.Forms.Views;
using MvvmCross.Platform;
using UIKit;
using PinPoint.Presenter;

namespace PinPoint.iOS.Presenter
{
    public class MvxCustomIosPresenter : MvxFormsIosViewPresenter
    {
        #region Constructor

        public MvxCustomIosPresenter(IUIApplicationDelegate applicationDelegate,
                                    UIWindow window, MvxFormsApplication formsApplication) : base(applicationDelegate, window, formsApplication)
        {
        }

        #endregion

        private IMvxFormsPagePresenter mFormsPagePresenter;
        public override MvvmCross.Forms.Views.IMvxFormsPagePresenter FormsPagePresenter
        {
            get
            {
                if (mFormsPagePresenter == null)
                {
                    mFormsPagePresenter = new MvxFormCustomPresenter(this.FormsApplication);
                    Mvx.RegisterSingleton(mFormsPagePresenter);
                }
                return mFormsPagePresenter;
            }
            set
            {
                mFormsPagePresenter = value;
            }
        }
    }
}
