﻿using System;
using System.Threading.Tasks;
using MvvmCross.Core.Navigation;
using MvvmCross.Core.ViewModels;
using PinPoint.Services;
using PinPoint.ViewModels.Base;
using PinPoint.Helpers;
using Xamarin.Forms;

namespace PinPoint.ViewModels
{
    public interface IAppBrowserView
    {
        void GoBack();

        void Refresh();

        void LoadUrl(string url);
    }

    public class AppBrowserAction
    {

    }


    public class AppBrowserViewModel : BaseWithObjectViewModel<string>
    {
        private readonly IApiService ApiService;

        private readonly IPlatformService PlatformService;

        public IAppBrowserView View { get; set; }

        #region Constructor

        public AppBrowserViewModel(IMvxNavigationService navigationService, IDialogService dialogService,
                                   IApiService apiService, IPlatformService platformService) : base(navigationService, dialogService)
        {
            this.ApiService = apiService;
            this.PlatformService = platformService;

            MessagingCenter.Subscribe<AppBrowserAction, string>(this, "Action", (sender, mid) =>
             {
                 AppSettings.Mid = String.Empty;
                 View?.LoadUrl($"https://mobile.vuteur.com/Account/LoginMobile?username={AppSettings.UserName}&password={AppSettings.Password}&tenancyName={AppSettings.TenancyName}&mid={mid}");
             });

        }

        #endregion

        #region Overrides

        public override void Prepare(string url)
        {
            base.Prepare();
            this.Source = url;
        }

        public override void ViewAppeared()
        {
            base.ViewAppeared();

            TryUpdateFireBaseToken();
        }

        #endregion

        #region Properties

        private string mSource;
        public string Source
        {
            get => mSource;
            set => SetProperty(ref mSource, value);
        }

        #endregion

        #region Commands

        public IMvxAsyncCommand GoBackCommand => new MvxAsyncCommand(GoBack);

        private async Task GoBack()
        {
            View?.GoBack();
        }

        public IMvxAsyncCommand HomeCommand => new MvxAsyncCommand(Home);

        private async Task Home()
        {
            View?.LoadUrl("https://mobile.vuteur.com/#/");
        }

        public IMvxAsyncCommand RefreshCommand => new MvxAsyncCommand(Refresh);

        private async Task Refresh()
        {
            View?.Refresh();
        }

        #endregion

        #region Methods


        async void TryUpdateFireBaseToken()
        {
            try
            {
                var result = await ApiService.SetToken(AppSettings.TenancyName, AppSettings.UserName,
                                                       AppSettings.Password, AppSettings.FirebaseToken);

                System.Diagnostics.Debug.WriteLine("UpdateFirebaseToken: " + result.ResponseObject.Success);
                this.PlatformService.Log("UpdateFirebaseToken", result.ResponseObject.Success.ToString());
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("UpdateFirebaseToken: Failed");
                this.PlatformService.Log("UpdateFirebaseToken", "Failed");
            }
        }

        public async void BackToLogin()
        {
            AppSettings.HasRegistered = false;
            AppSettings.UserName = String.Empty;
            AppSettings.Password = String.Empty;
            AppSettings.TenancyName = String.Empty;
            await ClearStackAndNavigateToPage<LoginViewModel>();
        }

        #endregion
    }
}
