﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using PinPoint.Pages.Base;
using PinPoint.ViewModels;
using System.Diagnostics;
using PinPoint.Helpers;

namespace PinPoint.Pages
{
    public partial class AppBrowserPage : BasePage<AppBrowserViewModel>, IAppBrowserView
    {

        void Webview_PropertyChanging(object sender, PropertyChangingEventArgs e)
        {
            if (e.PropertyName.Equals("Source"))
            {
                //this.ViewModel.ShowLoading();
                if (this.webView.Source is UrlWebViewSource source)
                {
                    Debug.WriteLine("Navigating: " + source.Url);
                    if (source.Url.StartsWith(AppConstants.LOGIN_URL + "?", StringComparison.OrdinalIgnoreCase) || source.Url.StartsWith(AppConstants.LOGOUT_URL, StringComparison.OrdinalIgnoreCase))
                    {
                        ViewModel.BackToLogin();
                    }
                }

            }
        }


        public AppBrowserPage()
        {
            InitializeComponent();

            //this.webView.Navigating += WebView_Navigating;
            this.webView.Navigating += WebView_Navigating;
            this.webView.Navigated += WebView_Navigated;
            if (Device.RuntimePlatform == Device.Android)
            {
                webView.PropertyChanging += Webview_PropertyChanging;
            }


        }

        public void GoBack()
        {
            //if (this.webView.HybridWebViewAction.CanGoBack())
            //{
            //    this.webView.HybridWebViewAction.GoBack();
            //}
            //else
            //{
            //    ViewModel.CloseCommand.Execute(null);
            //}

            if (this.webView.CanGoBack)
            {
                this.webView.GoBack();
            }
            else
            {
                ViewModel.CloseCommand.Execute(null);
            }
        }

        protected override void OnViewModelSet()
        {
            base.OnViewModelSet();
            this.ViewModel.View = this;
        }

        protected override void OnParentSet()
        {
            base.OnParentSet();

            if (this.Parent == null)
            {
                this.webView.Navigating -= WebView_Navigating;
                this.webView.Navigated -= WebView_Navigated;
                if (Device.RuntimePlatform == Device.Android)
                {
                    webView.PropertyChanging -= Webview_PropertyChanging;
                }
                this.webView.Navigating -= WebView_Navigating;
            }
        }

        protected override bool OnBackButtonPressed()
        {
            GoBack();
            return true;
        }

        #region Events

        void WebView_Navigating(string url)
        {
            Debug.WriteLine("Navigating: " + url);
            if (url.StartsWith(AppConstants.LOGIN_URL + "?", StringComparison.OrdinalIgnoreCase) || url.StartsWith(AppConstants.LOGOUT_URL, StringComparison.OrdinalIgnoreCase))
            {
                ViewModel.BackToLogin();
            }

        }

        void WebView_Navigated(object sender, WebNavigatedEventArgs e)
        {
            try
            {
                //this.ViewModel.HideLoading();
                Debug.WriteLine("Navigated: " + e.Url);
                if (Device.RuntimePlatform == Device.Android)
                {
                    if (e.Url.StartsWith(AppConstants.LOGIN_URL + "?", StringComparison.OrdinalIgnoreCase))
                    {
                        ViewModel.BackToLogin();
                    }
                }
            }
            catch
            {

            }
        }

        void WebView_Navigating(object sender, WebNavigatingEventArgs e)
        {
            try
            {
                Debug.WriteLine("Navigating: " + e.Url);
                if (e.Url.StartsWith(AppConstants.LOGIN_URL + "?", StringComparison.OrdinalIgnoreCase) || e.Url.StartsWith(AppConstants.LOGOUT_URL, StringComparison.OrdinalIgnoreCase))
                {
                    ViewModel.BackToLogin();
                }
                else
                {
                    // this.ViewModel.ShowLoading();
                }

            }
            catch
            {

            }
        }

        public void Refresh()
        {
            webView.Source = (webView.Source as UrlWebViewSource).Url;
            //webView.HybridWebViewAction?.Refresh();
        }

        public void LoadUrl(string url)
        {
            var urlSource = new UrlWebViewSource();
            urlSource.Url = url;
            //webView.Source = new UrlWebViewSource();
            webView.Source = urlSource;
            //webView.HybridWebViewAction?.LoadRequest(url);
        }

        #endregion
    }
}
