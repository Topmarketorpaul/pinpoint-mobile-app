﻿using System;
using Plugin.Settings;
using Plugin.Settings.Abstractions;

namespace PinPoint.Helpers
{
    public class AppSettings
    {
        private static ISettings Settings => CrossSettings.Current;

        #region AccessToken

        public static string AccessToken
        {
            get => Settings.GetValueOrDefault(nameof(AccessToken), String.Empty);
            set => Settings.AddOrUpdateValue(nameof(AccessToken), value);
        }

        #endregion

        #region TenancyName

        public static string TenancyName
        {
            get => Settings.GetValueOrDefault(nameof(TenancyName), String.Empty);
            set => Settings.AddOrUpdateValue(nameof(TenancyName), value);
        }

        #endregion

        #region UserName

        public static string UserName
        {
            get => Settings.GetValueOrDefault(nameof(UserName), String.Empty);
            set => Settings.AddOrUpdateValue(nameof(UserName), value);
        }

        #endregion

        #region Password

        public static string Password
        {
            get => Settings.GetValueOrDefault(nameof(Password), String.Empty);
            set => Settings.AddOrUpdateValue(nameof(Password), value);
        }

        #endregion

        #region HasRegistered

        public static bool HasRegistered
        {
            get => Settings.GetValueOrDefault(nameof(HasRegistered), false);
            set => Settings.AddOrUpdateValue(nameof(HasRegistered), value);
        }

        #endregion

        #region FirebaseToken

        public static string FirebaseToken
        {
            get => Settings.GetValueOrDefault(nameof(FirebaseToken), String.Empty);
            set => Settings.AddOrUpdateValue(nameof(FirebaseToken), value);
        }

        #endregion

        #region Mid

        public static string Mid
        {
            get => Settings.GetValueOrDefault(nameof(Mid), String.Empty);
            set => Settings.AddOrUpdateValue(nameof(Mid), value);
        }

        #endregion

        #region HasRegistered

        public static bool IsFirstTime
        {
            get => Settings.GetValueOrDefault(nameof(IsFirstTime), true);
            set => Settings.AddOrUpdateValue(nameof(IsFirstTime), value);
        }

        #endregion
    }
}
